global main
extern atoi
extern printf

section .data
	finishmsg: db 10, 0
	msgeven: db '(%d / 2) = %d ', 0
	msgodd: db '(%d * 3) + 1 = %d ', 0
	format: db '%d ', 0
	noparamerrormsg: db 'No parameter supplied.', 10, 0
	wrongparamerrormsg: db 'Parameter has to be an int32.', 10, 0

section .text
main:
	mov eax, [esp+4]	;Get number of Command
	cmp eax, 1		;Compare number of arguments to 1 (First Argument is always the filename)
	je errornoparam		;Jump to no param error output if argument number = 1
	jg readparam		;Jump to readparam if argument number > 1
readparam:
	mov eax, [esp+8]	;Get Command Line Argument List Start
	mov eax, [eax+4]	;Get 2nd Command Line Argument
	push eax		;Push eax
	call atoi		;Call Atoi
	add esp, 4		;Fix stack
	cmp eax, 0		;Compare to 0
	jle errorstring		;Jump to error output for wrong input
	jg print		;First run print
check:
	cmp eax, 1		;Compare eax to 1
	je finish		;Finish if eax == 1
	test eax, 1		;Test eax last bit to 1 (even or odd)
	jne odd			;If eax last bit == 0 go to odd
	je even			;If eax last bit == 1 go to even 
even:
	mov ebx, eax
	sar eax, 1		;Right shift by 1 is equal to division by 2
print_even:	
	push eax		;Push eax to stack
	push ebx		;Push ebx to stack
	push msgeven		;Push msgeven to stack
	call printf		;Print
	add esp, 8		;Move stackpoitner to eax
	pop eax			;Get eax from stack and move stackpointer back to where we started
	jmp check		;Go to print
odd:
	mov ebx, eax
	imul eax, 3		;Mulitply
	add eax, 1		;Add 1 to eax
print_odd:
	push eax		;Push eax to stack
	push ebx		;Push ebx to stack
	push msgodd		;Push msgodd to stack
	call printf		;Print
	add esp, 8		;Move stackpointer to eax
	pop eax			;Get eax from stack and move stackpointer back to where we started
	jmp check		;Go to print
print:
	push eax		;Push eax
	push format		;Push Format
	call printf		;Print
	add esp, 4		;Move stack pointer to eax
	pop eax			;Get eax from stack
	jmp check		;Go to check
finish:
	push finishmsg
	call printf
	add esp, 4
	ret			;Finish
errorstring:
	push wrongparamerrormsg
	call printf
	add esp, 4
	ret			;Finish
errornoparam:
	push noparamerrormsg	;Push error message
	call printf		;Print
	add esp, 4		;Fix stack
	ret			;Finish
